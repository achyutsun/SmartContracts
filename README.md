# gscplatform.io Smart Contracts

## ICO details...

There will be several stages of the token sale, each defined with the amount of tokens on offer and the associated bonus. 

All tokens that are not sold in each stage will be burned.

Token sale will have a hardcap and a softcap. 

## Token details

GSCP will be an ERC20 Ethereum token.

## About gscplatform.io Project

Here are the links:

* [Product website](https://gscplatform.io/)
* [ICO website](https://gscplatform.io/)
* [Telegram](https://t.me/GSCAviationOfficial)